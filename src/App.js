import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';

import RideForm from './RideForm';
import Requests from './Requests';

import Chinese from './language/chinese';

import './App.css';
function App() {
  return (
    <Box className="App" display="flex" justifyContent="center" alignItems="flex-start">
      <Stack display="flex" alignItems="center" justifyContent="center" style={{ width: '50vw' }}>
        <Box style={{ fontSize: '1.4rem', paddingTop: '50px', color: 'white' }}>
          UIUC Ride
        </Box>
        <RideForm />
        <Requests />
        <Box style={{ marginBottom: '10px', color: 'white' }}>{Chinese.helpText}</Box>
      </Stack>
    </Box>
  );
}

export default App;
