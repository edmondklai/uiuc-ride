import React from 'react';
import { useState, useEffect } from 'react';

import { format } from 'date-fns';

import Box from '@mui/material/Box';
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";

import ArrowRightIcon from '@mui/icons-material/ArrowRight';

import {
  collection, query, orderBy, limit, getDocs, where,
} from "firebase/firestore";
import { db } from "./firebase-config";
import Chinese from './language/chinese';

function Requests() {
  const [requests, setRequests] = useState();

  useEffect(() => {
    const fetchData = async () => {
      const requestsRef = collection(db, 'requests');
      const now = new Date();
      const q = query(requestsRef, where('date', '>', now), orderBy('date'), limit(10));
      const firestoreRequests = await getDocs(q);
      const req = []
      firestoreRequests.docs.forEach((doc) => {
        const data = doc.data()
        req.push(data);
      })
      setRequests(req);
    }
    fetchData();
  }, [])

  return (
    requests && requests.map((request) => {
      return <Card
        key={request.destination}
        variant="outlined"
        style={{ width: '350px', marginBottom: '10px' }}
        direction="column">
        <CardHeader title={
          <Box display="flex" alignItems="center" justifyContent="center">
            {request.origin}
            <ArrowRightIcon />
            {request.destination}
          </Box>
        }>
        </CardHeader>
        <CardContent>
          <Box>
            {request.type === 'request' ? `${Chinese.request} 🙏` : `${Chinese.provide} 💪`}
          </Box>
          <Box>
            {request.date ? format(request.date.toDate(), 'MM/dd/yy hh:mm a') : ''}
          </Box>
          <Box>
            {request.contact || ''}
          </Box>
        </CardContent>
      </Card >
    })
  )
}

export default Requests;