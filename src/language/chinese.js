const Chinese = {
  origin: '原点',
  destination: '目的地',
  provide: '提供',
  request: '请求',
  timeLeaving: '离开时间',
  contactAndInfo: '联系信息和附加信息',
  cancel: '取消',
  submit: '发送',
  submitted: '成功! 截图发送到UIUC Wall...',
  helpText: '有什么需要的可以发到UIUC Wall，我会看的'
}

export default Chinese;