import React, { useState } from "react";


import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import Stack from "@mui/material/Stack";
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';


import TextField from "@mui/material/TextField";
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';

import Chinese from './language/chinese';

import {
  collection, addDoc, serverTimestamp,
} from "firebase/firestore";
import { db } from "./firebase-config";

function RideForm() {
  const [open, setOpen] = useState(false);
  const [contact, setContact] = useState(false);
  const [origin, setOrigin] = useState('');
  const [destination, setDestination] = useState('');
  const [date, setDate] = useState(null);
  const [type, setType] = useState(null);
  const [status, setStatus] = useState('ready');

  const onClose = () => {
    setOpen(false);
  };

  const updateDate = (value) => {
    setDate(value);
  }

  const onSubmit = async () => {
    await addDoc(
      collection(
        db,
        "requests",
      ),
      {
        createdAt: serverTimestamp(),
        contact,
        origin,
        destination,
        date,
        type
      },
    );
    setOpen(false);
    setStatus('sent');
    setTimeout(() => {
      window.location.reload();
    }, 3000)
  };

  const onTypeChange = (event) => {
    setType(event.target.value);
  }

  return (
    <>
      <Stack>
        <Button style={{ height: '4rem', fontSize: '2rem' }} onClick={() => setOpen(true)}>📝</Button>
      </Stack>
      <Dialog open={open} onClose={onClose}>
        <DialogContent>
          <Box marginBottom="10px">
            <FormControl>
              <RadioGroup
                aria-labelledby="demo-radio-buttons-group-label"
                name="radio-buttons-group"
                row
                required
                value={type}
                onChange={onTypeChange}
              >
                <FormControlLabel value="request" control={<Radio />} label={Chinese.request} />
                <FormControlLabel value="provide" control={<Radio />} label={Chinese.provide} />
              </RadioGroup>
            </FormControl>
          </Box>
          <Stack direction="row">
            <Box marginBottom="30px">
              <TextField
                style={{ marginRight: '20px' }}
                autoFocus
                margin="dense"
                id="name"
                label={Chinese.origin}
                variant="standard"
                required
                value={origin}
                onChange={(e) => { setOrigin(e.target.value); }}
              />
            </Box>
            <Box marginBottom="30px">
              <TextField
                autoFocus
                margin="dense"
                id="name"
                label={Chinese.destination}
                variant="standard"
                required
                value={destination}
                onChange={(e) => { setDestination(e.target.value); }}
              />
            </Box>
          </Stack>
          <Box>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DateTimePicker
                label={Chinese.timeLeaving}
                value={date}
                onChange={updateDate}
                minutesStep={15}
                renderInput={(params) => <TextField {...params} />}
              />
            </LocalizationProvider>
          </Box>
          <Box marginBottom="30px">
            <TextField
              style={{ width: "500px" }}
              autoFocus
              margin="dense"
              id="name"
              label={Chinese.contactAndInfo}
              fullWidth
              required
              variant="standard"
              onChange={(e) => { setContact(e.target.value); }}
            />
          </Box>
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose}>{Chinese.cancel}</Button>
          <Button onClick={onSubmit}>{Chinese.submit}</Button>
        </DialogActions>
      </Dialog>
      <Snackbar
        open={status === "sent"}
        autoHideDuration={3000}
        onClose={() => { setStatus("ready"); }}
      >
        <Alert severity="success">{Chinese.submitted}</Alert>
      </Snackbar>
    </>
  );
}

export default RideForm;
