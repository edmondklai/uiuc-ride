// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDYt1FEortzJD366SvGbOA6HcQI8NEWPbE",
  authDomain: "ride-uiuc.firebaseapp.com",
  projectId: "ride-uiuc",
  storageBucket: "ride-uiuc.appspot.com",
  messagingSenderId: "1076185867487",
  appId: "1:1076185867487:web:563813f572f2c9f397b56a"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const db = getFirestore(app);
const auth = getAuth(app);

export { db, auth }